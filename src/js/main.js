window.addEventListener('load', function () {
  $('.left-topnav a').click(function (e) {
    e.preventDefault();
    $('.left-topnav a').removeClass('active');
    $(this).addClass('active');
  });

  // Create automatic function for binding all addEventListeners:
  const topnavs = Array.from(document.querySelectorAll('.topnav-links'));
  topnavs.forEach(topnav => {
    topnav.addEventListener('click', e => {
      openContent(e, e.target.hash.substr(1));
    });
  });

  var toomler = new Array(8).fill(false);
  function openContent (evt, attr) {
    var content, links;
    let i;
    content = document.getElementsByClassName('content-topnav');
    for (i = 0; i < content.length; i++) {
      content[i].style.display = 'none';
    }
    links = document.getElementsByClassName('topnav-links');
    if(Foundation.MediaQuery.atLeast("large")) {
      for (i = 0; i < links.length; i++) {
        links[i].className = links[i].className.replace(' active', '');
      }
      document.getElementById(attr).style.display = 'block';
    } else {
      evt.currentTarget.className += ' active';
      console.log(evt.currentTarget.className);
      toomlerMaintanence(evt.currentTarget, toomler);
    }
  }

  if(Foundation.MediaQuery.atLeast("large")) {
    document.getElementById('default-open').click();
  }

  const maincontents = Array.from(document.querySelectorAll('.release-links'));
  maincontents.forEach(maincontent => {
    maincontent.addEventListener('click', e => {
      openMain(e, e.target.hash.substr(1));
    });
  });

  // Create main-content tabs for switching:
  let mainCtn = document.getElementsByClassName('release-links');

  function openMain (evt, idName) {
    let content = document.getElementsByClassName('main-content');
    for (let i = 0; i < content.length; i++) {
      content[i].style.display = 'none';
    }
    for (let i = 0; i < mainCtn.length; i++) {
      mainCtn[i].className = mainCtn[i].className.replace(' active-links', '');
    }
    document.getElementById(idName).style.display = 'block';
    evt.currentTarget.className += ' active-links';
  }

  document.getElementById('default-link').click();

  // Access disabled attribute with js-functions:
  document.getElementsByName('date-picker')[0].onclick = function() {
    document.querySelector('input[name="date_chooser"]').disabled = true;
  }
  document.getElementsByName('date-picker')[1].onclick = function() {
    document.querySelector('input[name="date_chooser"]').disabled = false;
  }

  //Mini-toggle for RWD-submenu:
  document.getElementsByClassName("toggle-menu")[0].onclick = function () {
    this.classList.toggle("change");
    document.getElementsByClassName("left-topnav-ctn")[0].classList.toggle("toggle-ctn");
    document.getElementsByClassName("left-topnav")[0].classList.toggle("toggle-status");
  }

  function toomlerMaintanence(curClass, toom) {
    switch(curClass.className) {
      case 'topnav-links sub-releases active active':
        toom[0] = !toom[0]
        changeParam(toom[0], curClass);
        break;
      case 'topnav-links sub-orders active active':
        toom[1] = !toom[1]
        changeParam(toom[1], curClass);
        break;
      case 'topnav-links sub-bestsellers active active':
        toom[2] = !toom[2];
        changeParam(toom[2], curClass);
        break;
      case 'topnav-links sub-features active active':
        toom[3] = !toom[3];
        changeParam(toom[3], curClass);
        break;
      case 'topnav-links sub-merchandise active active':
        toom[4] = !toom[4];
        changeParam(toom[4], curClass);
        break;
      case 'topnav-links sub-genres active active':
        toom[5] = !toom[5];
        changeParam(toom[5], curClass);
        break;
      case 'topnav-links sub-labels active active':
        toom[6] = !toom[6];
        changeParam(toom[6], curClass);
        break;
      case 'topnav-links sub-sale active active':
        toom[7] = !toom[7];
        changeParam(toom[7], curClass);
        break;
    }
  }

  function changeParam(param, current) {
    if(param) {
      current.nextElementSibling.className += ' open-submenu';
      current.children[0].className += ' rotated';
    } else {
      current.nextElementSibling.classList.toggle('open-submenu', '');
      current.children[0].classList.toggle('rotated','');
    }
  }
});

